
import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class MainApp extends Application {

	private Stage primaryStage;

	@Override
	public void start(Stage primaryStage) {

		this.primaryStage = primaryStage;
		try {

			AnchorPane root = FXMLLoader.load(getClass().getResource("views/LoginForm.fxml"));
			Scene scene = new Scene(root);
			primaryStage.setScene(scene);

			//retire la Title Bar Windows
			//primaryStage.initStyle(StageStyle.UNDECORATED);

			//emp�che la fen�tre d'etre Resize
			primaryStage.setResizable(false);

			primaryStage.setTitle("Connexion Logiciel Gestion Vide Grenier");
			primaryStage.show();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}

}
