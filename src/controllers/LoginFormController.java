package controllers;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class LoginFormController {

    @FXML
    private TextField field_username;

    @FXML
    private PasswordField field_password;

    @FXML
    private Button btn_login;

    @FXML
    private Button btn_close;

    @FXML
    void Action_Login(ActionEvent event) {
    	CreateMsgBox("Connexion Impossible", "Maintenance en cours", "Vous ne pouvez vous connectez pour l'instant");
    }

    @FXML
    void Action_close(ActionEvent event) {
    	Platform.exit();
    }

    private void CreateMsgBox(String Title, String Header, String Content) {
    	Alert dialogE = new Alert(AlertType.ERROR);
    	dialogE.setTitle(Title);
    	dialogE.setHeaderText(Header);
    	dialogE.setContentText(Content);
    	dialogE.showAndWait();
    }

}
